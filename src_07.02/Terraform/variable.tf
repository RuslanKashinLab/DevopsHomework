variable "yc_token" {
  type        = string
  default = "AQAAAAAD3***********"
}

variable "yc_zone" {
  type        = string
  default = "ru-central1-a"
}

variable "yc_cloud_id" {
  type        = string
  default = "b1g8iii5fc0rhcs2hhva"
}

variable "yc_folder_id" {
  type        = string
  default = "b1gpksnu88qbtk830p1h"
}
